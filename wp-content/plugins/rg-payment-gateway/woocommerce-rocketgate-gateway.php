<?php
/*
Plugin Name: WooCommerce Rocketgate Gateway
Plugin URI: https://progressus.io
Description: Extends WooCommerce with Rocketgate gateway.
Version: 1.0
Author: Progressus
Author URI: https://progressus.io/
	Copyright: © 2009-2011 WooThemes.
	License: GNU General Public License v3.0
	License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/
add_action('plugins_loaded', 'woocommerce_gateway_rocketgate_init', 0);
function woocommerce_gateway_rocketgate_init() {
	if ( !class_exists( 'WC_Payment_Gateway' ) ) return;
	/**
 	 * Localisation
	 */
	load_plugin_textdomain('wc-rocketgate-gateway', false, dirname( plugin_basename( __FILE__ ) ) . '/languages');

	define ('PAYMENT_GATEWAY_ID', 'rocketgate-gateway' );
	define ('PLUGIN_DIR', plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) . '/' );
	define ('PLUGIN_PATH', plugin_dir_path( __FILE__ ).'/');

	require_once( PLUGIN_PATH . 'includes/class-wc-rocketgate-gateway.php' );
	require_once( PLUGIN_PATH . 'includes/class-rg-gateway-call.php' );
	require_once( PLUGIN_PATH . 'includes/class-rg-gateway-logger.php' );
	
	add_action( 'init', 'rg_plugin_init');
	add_filter( 'woocommerce_payment_gateways', 'woocommerce_add_gateway_rocketgate_gateway' );
	add_filter( 'woocommerce_default_address_fields', 'adjust_requirement_of_checkout_address_fields' );
	add_filter( 'woocommerce_billing_fields', 'adjust_requirement_of_checkout_contact_fields');
}

function rg_plugin_init(){

	new RG_Gateway_Call();
}

function adjust_requirement_of_checkout_address_fields( $fields ) {
	$fields['first_name']['required']   = false;
	$fields['last_name']['required']    = false;
	$fields['company']['required']      = false;
	$fields['country']['required']      = false;
	$fields['address_1']['required']    = false;
	$fields['address_2']['required']    = false;
	$fields['city']['required']         = false;
	$fields['state']['required']        = false;
	$fields['postcode']['required']     = false;

	return $fields;
}
function adjust_requirement_of_checkout_contact_fields( $fields ) {
	$fields['billing_phone']['required']    = false;
	$fields['billing_email']['required']    = false;

	return $fields;
}

/**
 * Add the Gateway to WooCommerce
 **/
function woocommerce_add_gateway_rocketgate_gateway($methods) {
	$methods[] = 'WC_Rocketgate_Gateway';
	return $methods;
}