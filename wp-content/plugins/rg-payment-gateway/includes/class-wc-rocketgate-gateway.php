<?php

/**
 * Gateway class
 */
class WC_Rocketgate_Gateway extends WC_Payment_Gateway {

	function __construct() {

		// Register plugin information
		$this->id			    = PAYMENT_GATEWAY_ID;
		$this->has_fields = true;

		// The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
		$this->method_title = __( "Rocketgate Gateway", 'wc-rocketgate-gateway' );

		// The description for this Payment Gateway, shown on the actual Payment options page on the backend
		$this->method_description = __( "Rocketgate Payment Gateway Plug-in for WooCommerce", 'wc-rocketgate-gateway' );

		// The title to be used for the vertical tabs that can be ordered top to bottom
		$this->title = __( "Rocketgate Gateway", 'wc-rocketgate-gateway' );

		// If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
		$this->icon = null;
		$this->supports   = array(
						'products',
						'refunds'
						);

		// Create plugin fields and settings
		$this->init_form_fields();
		$this->init_settings();

		// Get setting values
		foreach ( $this->settings as $key => $val ) $this->$key = $val;

		// Load plugin checkout icon
		$this->icon = PLUGIN_DIR . 'images/blank.png';
		
		// Add hooks
		add_action( 'admin_notices',                                            array( $this, 'rocketgate_commerce_ssl_check' ) );
		add_action( 'woocommerce_update_options_payment_gateways',              array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
	}

	/**
	 * Check if SSL is enabled and notify the user.
	 */
	function rocketgate_commerce_ssl_check() {
		if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'no' && $this->enabled == 'yes' ) {
				echo '<div class="error"><p>' . sprintf( __('Rocketgate Gateway is enabled and the <a href="%s">force SSL option</a> is disabled; your checkout is not secure! Please enable SSL and ensure your server has a valid SSL certificate.', 'woothemes' ), admin_url( 'admin.php?page=woocommerce' ) ) . '</p></div>';
				}
	}

	/**
	 * Initialize Gateway Settings Form Fields.
	 */
	function init_form_fields() {

		$log_path = admin_url('admin.php?page=wc-status&tab=logs');

		$this->form_fields = array(
			'enabled'     => array(
				'title'       => __( 'Enable/Disable', 'wc-rocketgate-gateway' ),
				'label'       => __( 'Enable Rocketgate Gateway', 'wc-rocketgate-gateway' ),
				'type'        => 'checkbox',
				'description' => '',
				'default'     => 'no'
			),
			'title'       => array(
				'title'       => __( 'Title', 'wc-rocketgate-gateway' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'wc-rocketgate-gateway' ),
				'default'     => __( 'Rocketgate Gateway', 'wc-rocketgate-gateway' )
			),
			'description' => array(
				'title'       => __( 'Description', 'wc-rocketgate-gateway' ),
				'type'        => 'textarea',
				'description' => __( 'This controls the description which the user sees during checkout.', 'wc-rocketgate-gateway' ),
				'default'     => __('Pay securely by Credit or Debit Card through Rocketgate Secure Servers.', 'wc-rocketgate-gateway' )
			),
			'merchant_id'     => array(
				'title'        => __('Merchant ID', 'wc-rocketgate-gateway' ),
				'type'         => 'text',
				'required'    => true,
			  
				'description'  => __('Your Merchant Id from Rocketgate, for this plugin', 'wc-rocketgate-gateway' )
			),
			'merchant_password'     => array(
				'title'        => __('Merchant Password', 'wc-rocketgate-gateway' ),
				'type'         => 'text',
				'required'    => true,
			  
				'description'  => __('Your merchant password from Rocketgate, for this plugin', 'wc-rocketgate-gateway' )
			),
			'hash_secret' => array(
				'title'        => __('Hash Secret', 'wc-rocketgate-gateway' ),
				'type'         => 'text',
				'description'  =>  __('Hash Secret from RocketGate', 'wc-rocketgate-gateway' )
			),
			'env_mode'      => array(
				'title'        => __('Environment Mode', 'wc-rocketgate-gateway' ),
				'type'         => 'select',
				'label'        => __('Environment Mode for Rocketgate Payment Module.', 'wc-rocketgate-gateway' ),
				'default'      => 'sandbox',
				'options'     => array(
					'sandbox' 		=> __( 'Sandbox', 'wc-rocketgate-gateway' ),
					'production' 	=> __( 'Production', 'wc-rocketgate-gateway' )
				)
			),
			'customer_prefix' => array(
				'title'        => __('Customer Prefix', 'wc-rocketgate-gateway' ),
				'type'         => 'text',
				'description'  =>  __('Customer prefix for user id', 'wc-rocketgate-gateway' ),
				'default' 		=> 'woo_',
			),
			'rg_debug' => array(
				'title'             => __( 'Debug Log', 'pr-shipping-dhl' ),
				'type'              => 'checkbox',
				'label'             => __( 'Enable logging', 'pr-shipping-dhl' ),
				'default'           => 'yes',
				'description'       => sprintf( __( 'A log file containing the communication to the RocketGate server will be maintained if this option is checked. This can be used in case of technical issues and can be found %shere%s.', 'wc-rocketgate-gateway' ), '<a href="' . $log_path . '" target = "_blank">', '</a>' )
			),
		);
	}

	/**
	 * UI - Admin Panel Options
	 */
	function admin_options() { ?>
		<h3><?php _e( 'Rocketgate Gateway','woothemes' ); ?></h3>
			<p><?php _e( 'The Rocketgate Gateway is simple and powerful.', 'wc-rocketgate-gateway' ); ?></p>
			<table class="form-table">
			<?php $this->generate_settings_html(); ?>
		</table>
	<?php }

	public function enqueue_scripts(){
		
		if( $this->env_mode == 'production' ){
			//$base_url = 'https://secure.rocketgate.com/hostedpage/EmbeddedFormScripts.jsp';
			$base_url = 'https://secure.rocketgate.com/hostedpage/EmbeddedFields.jsp';
		}else{
			//$base_url = 'https://dev-secure.rocketgate.com/hostedpage/EmbeddedFormScripts.jsp';
			$base_url = 'https://dev-secure.rocketgate.com/hostedpage/EmbeddedFields.jsp';
		}

		$full_url 	= add_query_arg( $this->get_args_with_hash(), $base_url );

		wp_enqueue_script( 'rg-gateway-embedded-form', $full_url );
		wp_enqueue_script( 'rg-gateway-script', PLUGIN_DIR . 'assets/js/rg-gateway.js', array(), '1.0.0' );
		wp_localize_script( 'rg-gateway-script', 'rg_var', $this->get_iframe_parameter() );
		wp_localize_script( 'rg-gateway-script', 'rg_url', array( 
			'base_url' => $base_url,
			'iframe_url' => $this->get_iframe_base_url(),
			'ajax_url' 	=> admin_url( 'admin-ajax.php' ),
		) );

		/*<script type="text/javascript" src="<?php echo esc_url( $full_url ); ?>"></script>*/
		
	}

	public function enqueue_styles(){
		wp_enqueue_style( 'rg-gateway-style', PLUGIN_DIR . 'assets/css/rg-gateway.css' );
	}

	public function get_iframe_parameter(){

		$args 				= $this->get_rocketgate_args();
		
		unset( $args['time'] );

		$args['env_mode'] 	= $this->env_mode;

		return $args;
	}

	public function get_iframe_base_url(){

		return PLUGIN_DIR . 'includes/iframe/paymentgateway.php';
		
	}

	public function get_iframe_url(){
		$base_url 			= $this->get_iframe_base_url();
		$args 				= $this->get_iframe_parameter();
		
		$full_url 			= add_query_arg( $args, $base_url );
		
		return $full_url;
	}

	/**
	 * UI - Payment page fields for Rocketgate Payment Gateway.
	 */
	public function payment_fields() {
		// Description of payment method from settings
		if ( $this->description ) {
			echo '<p>'. $this->description .'</p>';
		}
		?>
		<script type="text/javascript">
			function rg_fill_hidden_value( the_id, the_value ){

				var el = document.getElementById( the_id );
				
				if( el != null ){
					document.getElementById( the_id ).value = the_value;
				}

			}

			function rg_get_wc_checkout_form(){
				var x = document.getElementsByClassName("woocommerce-checkout");
				var i;
				for (i = 0; i < x.length; i++) {
					return x[i];
					break;
				}

				return false;
			}

			function rg_block_form(){

				var wc_checkout_form = rg_get_wc_checkout_form();
				wc_checkout_form.block({
						message: null,
						overlayCSS: {
							background: '#fff',
							opacity: 0.6
						}
				});
			}

			function rg_unblock_form(){
				var wc_checkout_form = rg_get_wc_checkout_form();
				wc_checkout_form.unblock();
			}
			
			function rg_place_order_click(){
				var place_order = document.getElementById( 'place_order' );
				place_order.click();
			}
		</script>
		<iframe id="rg_iframe_form" src="<?php echo esc_url( $this->get_iframe_url() ); ?>" title="<?php _e('Rocket Gate Payment Form', 'wc-rocketgate-gateway'); ?>" onload="rg_resize_iframe( this );" width="100%" style="min-height: 330px;" scrolling="no"></iframe>
		<img class="img-fluid" src="http://ketosswp.com/wp-content/uploads/2020/10/cards.png" style="max-width: 220px; display: block; margin: 0 auto;">
		<input type="hidden" name="rg_result_guid" id="rg_result_guid" value="" />
		<input type="hidden" name="rg_result_card_hash" id="rg_result_card_hash" value="" />
		<input type="hidden" name="rg_result_auth_no" id="rg_result_auth_no" value="" />
		<input type="hidden" name="rg_result_card_bin" id="rg_result_card_bin" value="" />
		<input type="hidden" name="rg_result_rocketpay_indicator" id="rg_result_rocketpay_indicator" value="" />
		<input type="hidden" name="rg_result_merchant_account" id="rg_result_merchant_account" value="" />
		<?php 
	}

	public function get_rocketgate_args() {

		$user 		= wp_get_current_user();
	
		if( isset( $user->ID ) && $user->ID != 0 ){
			$user_ID = $user->ID;
		}else{
			$user_ID = "guest_" . time();
		}

		$lang = get_locale();
		if( isset( $_SESSION['lang'] ) ){
			$lang = $_SESSION['lang'];
		}

		// RockeGate Args
		$args 	= array(
			'merch' 	=> $this->merchant_id,
			'id' 		=> $this->customer_prefix . $user_ID, //harcoded-progressus
			'purchase' 	=> 'TRUE',
			'lang' 		=> $lang,
			'currency' 	=> get_woocommerce_currency(),
			'time' 		=> time(),
		);

		if( isset( $user->user_email ) ){
			$args['email'] 		= $user->user_email;
		}

		$args['secret'] = $this->hash_secret;

		$args = apply_filters( 'woocommerce_rocketgate_gateway_args', $args );

		return $args;
	}

	public function get_args_with_hash(){

		$args = $this->get_rocketgate_args();

		//
		// hash the unencoded string and return the raw output
		//
		$param_string = add_query_arg( $args, '' );
		$param_string = str_replace( '?', '', $param_string );
		$sha1Hash = hash("sha1", $param_string, true);
	
		//
		// base64 encode the hash output
		//
		$b64 = base64_encode($sha1Hash);

		//
		// prepare the encoded string to return
		//

		unset( $args['secret']);
		$args['hash'] = urlencode( $b64 );
		
		return $args;
	}

	/**
	 * Process the payment and return the result.
	 */
	function process_payment( $order_id ) {

		$order = wc_get_order( $order_id );

		// Mark as on-hold (we're awaiting the payment)
		$order->update_status( 'processing', __( 'Processing order', 'wc-gateway-offline' ) );
            
		// Reduce stock levels
		wc_reduce_stock_levels( $order_id );

		// Remove cart
		WC()->cart->empty_cart();

		// Return thank you redirect
		return array(
			'result' => 'success',
			'redirect' => $this->get_return_url( $order )
					
		);

	}
}