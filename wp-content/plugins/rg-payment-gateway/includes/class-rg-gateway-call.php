<?php 
require PLUGIN_PATH . 'includes/rocketgate-php-sdk/vendor/autoload.php';

use RocketGate\Sdk\GatewayRequest;
use RocketGate\Sdk\GatewayResponse;
use RocketGate\Sdk\GatewayService;

/**
 * Gateway class
 */
class RG_Gateway_Call{

	public $rg_gateway;

	protected $rg_logger;

	function __construct() {

		$this->rg_gateway = WC()->payment_gateways->payment_gateways()[ PAYMENT_GATEWAY_ID ];

		$rg_debug = isset( $this->rg_gateway->rg_debug ) ? $this->rg_gateway->rg_debug : 'yes';
		$this->rg_logger = new RG_Gateway_Logger( $rg_debug );

		add_action( 'wp_ajax_rg_gateway_request', array( $this, 'gateway_request') );
		add_action( 'wp_ajax_nopriv_rg_gateway_request', array( $this, 'gateway_request') );
		add_action( 'woocommerce_checkout_order_processed', array( $this, 'add_rg_result_to_order' ), 10, 3 );
	}

	public function get_args(){
		
		$user 		= wp_get_current_user();
	
		if( isset( $user->ID ) && $user->ID != 0 ){
			$user_ID 	= $user->ID;
		}else{
			$user_ID 	= "guest_" . time();
		}

		$args = array(
			'merch' 	=> $this->rg_gateway->merchant_id,
			'password' 	=> $this->rg_gateway->merchant_password,
			'id' 		=> $this->rg_gateway->customer_prefix . $user_ID, //harcoded-progressus
			'site_id' 	=> 1,
			'amount' 	=> WC()->cart->get_total( 'noview' ),
			'currency' 	=> get_woocommerce_currency(),
			'purchase' 	=> 'TRUE',
			'time' 		=> time(),
			'secret' 	=> $this->rg_gateway->hash_secret
		);

		return $args;
	}

	public function add_rg_result_to_order( $order_id, $posted_data, $order ){

		foreach( $_REQUEST as $var => $value ){
			if( strpos( $var, 'rg_result_' ) !== false ){
				$order->update_meta_data( $var, $_REQUEST[ $var ] );
			}
		}
		$order->save();
		
	}

	public function gateway_request(){

		if( !isset( $_REQUEST['submit_token'] ) ){
			exit('no cheating');
		}

		//error_log('gateway_request');
		//
		//	Allocate the objects we need for the test.
		//
		$request = new GatewayRequest();
		$response = new GatewayResponse();
		$service = new GatewayService();

		//
		//	Setup the Auth-Only request.
		//

		$args 	= $this->get_args();
		$token 	= $_REQUEST['submit_token'];

		$request->Set(GatewayRequest::MERCHANT_ID(), $args['merch'] );
		$request->Set(GatewayRequest::MERCHANT_PASSWORD(), $args['password']);

		$time = time();
		$request->Set(GatewayRequest::MERCHANT_CUSTOMER_ID(), $args['id'] );
		$request->Set(GatewayRequest::MERCHANT_SITE_ID(), $args['site_id'] );
		//$request->Set(GatewayRequest::EMAIL(), $args['email'] );
		
		$request->Set(GatewayRequest::AMOUNT(), $args['amount'] );
		$request->Set(GatewayRequest::CURRENCY(), $args['currency'] );
		$request->Set(GatewayRequest::EMBEDDED_FIELDS_TOKEN(), $token );

		//
		//	Setup test parameters in the service and
		//	request.
		//
		if( $this->rg_gateway->env_mode == 'sandbox' ){
			// error_log('sandbox');
			$service->SetTestMode(TRUE);
		}

		//error_log(print_r($request,true));
		//print_r($request);
		//
		//	Perform the Auth-Only transaction.
		//

		$this->rg_logger->write( "PerformPurchase" );
		$this->rg_logger->write( print_r($request, true) );
		// $this->rg_logger->write( print_r($service, true) );

		$result = array();

		if ($service->PerformPurchase($request, $response)) {
		//if ($service->PerformAuthOnly($request, $response)) {
			$this->rg_logger->write( print_r($response, true) );

			$result['result'] 			= true;
			$result['response_code'] 	= $response->Get(GatewayResponse::RESPONSE_CODE());
			$result['reason_code'] 		= $response->Get(GatewayResponse::REASON_CODE());
			$result['auth_no'] 			= $response->Get(GatewayResponse::AUTH_NO());
			$result['avs'] 				= $response->Get(GatewayResponse::AVS_RESPONSE());
			$result['cvv2'] 			= $response->Get(GatewayResponse::CVV2_CODE());
			$result['merchant_account'] = $response->Get(GatewayResponse::MERCHANT_ACCOUNT());
			$result['guid'] 			= $response->Get(GatewayResponse::TRANSACT_ID());
			$result['scrub'] 			= $response->Get(GatewayResponse::SCRUB_RESULTS());

			$this->rg_logger->write( "PerformLookup" );
			$this->rg_logger->write( print_r($request, true) );

			if( $service->PerformLookup( $request, $response ) ){

				$this->rg_logger->write( print_r($response, true) );

				$result['card_hash'] 			= $response->Get(GatewayResponse::CARD_HASH());
				$result['card_bin'] 			= $response->Get(GatewayResponse::CARD_BIN());
				$result['rocketpay_indicator'] 	= $response->Get(GatewayResponse::ROCKETPAY_INDICATOR());
				$result['email'] 				= $response->Get(GatewayResponse::EMAIL());
			} else {
				$this->rg_logger->write( print_r($response, true) );
			}

		} else {

			$this->rg_logger->write( print_r($response, true) );

			$result['result'] 			= false;
			$result['response_code'] 	= $response->Get(GatewayResponse::RESPONSE_CODE());
			$result['reason_code'] 		= $response->Get(GatewayResponse::REASON_CODE());
			$result['reason_text'] 		= $this->get_reason_text( $result['reason_code'] );
			$result['exception'] 		= $response->Get(GatewayResponse::EXCEPTION());
			$result['guid'] 			= $response->Get(GatewayResponse::TRANSACT_ID());
			$result['scrub'] 			= $response->Get(GatewayResponse::SCRUB_RESULTS());
		}

		wp_send_json( $result );
	}

	public function get_reason_text( $reason_code ){

		$tran_error = array(
			'0' => "Transaction Successful",
			'100' => 'No matching transaction',
			'101' => 'A void operation cannot be performed because the original transaction has already been voided, credited, or settled.',
			'102' => 'A credit operation cannot be performed because the original transaction has already been voided, credited, or has not been settled.',
			'103' =>'A ticket operation cannot be performed because the original auth-only transaction has been voided or ticketed.',
			'104' => 'The bank has declined the transaction.',
			'105' => 'The bank has declined the transaction because the account is over limit.',
			'106' => 'The transaction was declined because the security code (CVV) supplied was invalid.',
			'107' => 'The bank has declined the transaction because the card is expired.',
			'108' => 'The bank has declined the transaction and has requested that the merchant call.',
			'109' => 'The bank has declined the transaction and has requested that the merchant pickup the card.',
			'110' => 'The bank has declined the transaction due to excessive use of the card.',
			'111' => 'The bank has indicated that the account is invalid.',
			'112' => 'The bank has indicated that the account is expired.',
			'113' => 'The issuing bank is temporarily unavailable. May be tried again later.',
			'117' => 'The transaction was declined because the address could not be verified.',
			'123' => 'The Rebill transaction was declined because the user asked their bank to stop the rebill. It is suggested that merchants cancel subscriptions when they are managing their own rebilling.',
			'129' => 'Declined with Incorrect PIN or Pin tried exceeded.',
			'150' => 'The transaction was declined because the address could not be verified.',
			'151' => 'The transaction was declined because the security code (CVV) supplied was invalid.',
			'152' => 'The TICKET request was for an invalid amount. Please verify the TICKET for less then the AUTH_ONLY.',
			'154' => 'The transaction was rejected because it didn’t pass validation of supplied parameters', 	# Risk Fail
			'155' => 'Declined for 3DSecure Authentication', 	# Risk Fail
			'156' => 'Transaction was declined because the bank returned an Unsupported CardType error.', 	# Risk Fail
			'157' => 'Transaction was declined because the bank finds risky', 	# Risk Fail
			'161' => 'Transaction was declined because of previous hard declines on the same card #', 	# Risk Fail
			'162' => 'Transaction was declined by acquiring bank for reaching account limit.', 	# Risk Fail
			'163' => 'Declined for 3DSecure Authentication', 	# Risk Fail
			'164' => 'The bank has declined the transaction and advised this card was stolen', 	# Risk Fail
			'200' => 'Transaction was declined', 	# Risk Fail
			'201' => 'Transaction was declined',	# Customer blocked
			'202' => 'The card is enrolled in 3-D Secure. Please refer to 3D-Secure Programmers Guide.',	# Customer blocked
			'203' => 'The card is eligible to participate in 3-D Secure, however, it has not been enrolled. The transaction can be discarded or competed as described in section 6 of the 3D-Secure Guide',	# Customer blocked
			'204' => 'The card is not eligible to participate in 3-D Secure. The transaction can be discarded or competed as described in section 7 of the 3D-Secure Guide',	# Customer blocked
			'205' => 'The system can not determine whether or not the card is enrolled in 3-D Secure. The transaction can be discarded or competed as described in section 8 of the 3D-Secure Guide',	# Customer blocked
			'208' => 'Transaction was declined because customer has a duplicate membership matching this customer id',	# Customer blocked
			'209' => 'Transaction was declined because customer has a duplicate membership matching this card #',	# Customer blocked
			'210' => 'Transaction was declined because customer has a duplicate membership matching this email',	# Customer blocked
			'211' => 'Transaction was declined because the amount exceeded the Max configured purchase amount',	# Customer blocked
			'212' => 'Transaction was declined because the card # and $ amount matched another transaction in the configured time amount.',	# Customer blocked
			'213' => 'Transaction was declined because customer has breached the velocity limits matched by Customer ID',	# Customer blocked
			'214' => 'Transaction was declined because customer has breached the velocity limits matched by card #',	# Customer blocked
			'215' => 'Transaction was declined because customer has breached the velocity limits matched by Email',	# Customer blocked
			'216' => 'Transaction was declined because Iovation declined due to risk match',	# Customer blocked
			'217' => 'Transaction was declined because customer has breached the velocity limits matched by Iovation Device',	# Customer blocked
			'218' => 'Transaction was declined because customer has a duplicate membership matching this Iovation Device',	# Customer blocked
			'219' => 'Transaction was declined by CheckAcctCompromised scrub for potential account compromise as source IP or device was different from previous transactions',	# Customer blocked
			'220' => 'Transaction was declined by NewCardScrubLimit scrub for a customer adding too many new cards in a given period.',	# Customer blocked
			'221' => 'Transaction was declined due to the affiliate id black listing',	# Customer blocked
			'222' => 'Transaction was declined by LimitTrials scrub for a user signing up repeatedly for the same trial',	# Customer blocked
			'223' => '3DS lookup generated a bypass which indicates you should re-submit as non- 3ds',	# Customer blocked
			'224' => 'Transaction was declined by NewCardRequiresDevice scrub for a new cards with a missing Iovation device ID',	# Customer blocked
			'300' => 'A DNS failure has prevented the merchant application from resolving gateway host names.',
			'301' => 'The merchant application is unable to connect to an appropriate host.',
			'302' => 'An error occurred while transmitting data to the RocketGate servers.',
			'303' => 'A timeout occurred while waiting for a transaction response from the gateway servers.',
			'304' => 'An error occurred while reading the response from the RocketGate servers.',
			'305' => 'Service Unavailable',
			'307' => 'Unexpected/Internal Error',
			'311' => 'Bank Communications Error',
			'312' => 'Bank Communications Error',
			'313' => 'Bank Communications Error',
			'314' => 'Bank Communications Error',
			'315' => 'Bank Communications Error',
			'316' => 'Bank Communications Error',
			'321' => 'Bank Communications Error',
			'322' => 'Bank Communications Error',
			'323' => 'Bank Communications Error',
			'325' => 'Internal error or setup issue with 3D Secure MPI communication.',
			'326' => 'Error is returned during datacenter maintenence to trigger the gateway client to resubmit the request to an alternate RocketGate datacenter so no interruption occurs',
			'400' => "Invalid XML",
			'402' => "Invalid Transaction",
			'403' => 'Invalid Card Number',
			'404' => 'Invalid Expiration',
			'405' => 'Invalid Amount',
			'406' => 'Invalid Merchant ID',
			'407' => 'Invalid Merchant Account',
			'408' => 'The merchant account specified in the request is not setup to accept the card type included in the request.',
			'409' => 'No Suitable Account',
			'410' => 'Invalid Transact ID',
			'411' => 'Invalid Access Code',
			'412' => 'Invalid Customer Data Length',
			'413' => 'Invalid External Data Length',
			'414' => 'Invalid Customer ID',
			'418' => 'Invalid Currency',
			'419' => 'Incompatible Currency',
			'420' => 'Invalid Rebill Arguments',
			'421' => 'Invalid Phone',
			'422' => 'Invalid Country Code',
			'436' => 'Incompatible Descriptors',
			'437' => 'Invalid Referral Data',
			'438' => 'Invalid Site ID',
			'443' => 'Transaction Declined, Invalid Request. Please contact support',
			'444' => 'Transaction Declined, Invalid Request. Please contact support',
			'445' => 'Transaction Declined, Invalid Request. Please contact support',
			'446' => 'Transaction Declined, Invalid Request. Please contact support',
			'447' => 'Missing the PARES value in a 3-D Secure transaction.',
			'448' => 'The membership record was found but not active. For example, you’d hit this error when submitting a PerformRebillCancel() request and the account is already canceled.',
			'449' => 'CVV2_CHECK has been requested but the CVV was > 4 chars',
			'450' => 'Improper 3D Secure Data',
			'451' => 'Invalid Clone Data',
			'452' => 'Redundant Suspend/Resume',
			'453' => 'Invalid PayInfo Transact ID',
			'454' => 'Invalid Capture Days',
			'456' => 'Session Unavailable',
			'457' => 'Invalid Token',
		);

		if( isset( $tran_error[ $reason_code ] ) ){
			return $tran_error[ $reason_code ];
		}else{
			return _e('Error unknown', 'wc-rocketgate-gateway' );
		}
	}
}