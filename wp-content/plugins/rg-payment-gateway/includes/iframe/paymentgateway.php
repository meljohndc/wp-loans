<?php 

function get_args_with_hash( $args ){

	//
	// hash the unencoded string and return the raw output
	//

	$param_string 	= parse_array_to_get_string( $args );
	$sha1Hash 		= hash("sha1", $param_string, true);
	
	//
	// base64 encode the hash output
	//
	$b64 = base64_encode($sha1Hash);

	//
	// prepare the encoded string to return
	//

	unset( $args['secret']);
	$args['hash'] = urlencode( $b64 );
	
	return $args;
}

function parse_array_to_get_string( $args ){

	$args_string = array();
	foreach( $args as $var => $value ){
		$args_string[] = $var . '=' . $value;
	}

	$param_string 	= implode( '&', $args_string );

	return $param_string;
}

	if( $_GET['env_mode'] == 'production' ){
		//$base_url = 'https://secure.rocketgate.com/hostedpage/EmbeddedFormScripts.jsp';
		$base_url = 'https://secure.rocketgate.com/hostedpage/EmbeddedFields.jsp';
	}else{
		//$base_url = 'https://dev-secure.rocketgate.com/hostedpage/EmbeddedFormScripts.jsp';
		$base_url = 'https://dev-secure.rocketgate.com/hostedpage/EmbeddedFields.jsp';
	}

	unset( $_GET['env_mode'] );
	unset( $_GET['amount'] );

	$args 			= $_GET;

	$args['lang'] 	= strtoupper( substr($args['lang'], 0, 2 ) );
	$args['time'] 	= time();

	$secret 		= $args['secret'];
	unset( $args['secret'] );
	$args['secret'] = $secret;
	$args 			= get_args_with_hash( $args );
	$args_string 	= parse_array_to_get_string( $args );
	$embedfields_url = $base_url . '?' . $args_string;
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Rocket Gate Payment Gateway</title>
    <link rel="stylesheet" href="../../assets/css/rg-gateway.css" />
	<script type="text/javascript" src="<?php echo $embedfields_url; ?>"></script>
	<script type="text/javascript" src="../../assets/js/jquery-3.5.1.min.js"></script>
	<script type="text/javascript" src="../../assets/js/rg-gateway.js"></script>
  </head>

  <body>
	<div id="rg_gateway_wrapper">
		<div id="formWrapper">
		<form id="MainForm" method="POST">
			<div id="rgCardData">
				<script type="text/javascript">RocketGateLoadFields('rgCardData', 'cardNo', 'errors');</script>
			</div>

			<script type="text/javascript">RocketGateConnectFields("MainForm"); </script>
			<div id="rgSubmit">
				<button id="rgSubmitButton" onclick="rg_submit_func( this )">
					<span class="bigtext-btn"><?php echo 'Rush My Order'; ?></span>
					<span class="smalltext-btn"><?php echo 'Enjoy Your Package!'; ?></span>
				</button>
			</div>
		</form>
		</div>
	</div>
  </body>
</html>