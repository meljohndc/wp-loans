<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfab637627a3debb65828c9ab6d9a4b06
{
    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'RocketGate\\Sdk\\Tests\\' => 21,
            'RocketGate\\Sdk\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'RocketGate\\Sdk\\Tests\\' => 
        array (
            0 => __DIR__ . '/../..' . '/tests',
        ),
        'RocketGate\\Sdk\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'RocketGate\\Sdk\\GatewayChecksum' => __DIR__ . '/../..' . '/src/GatewayChecksum.php',
        'RocketGate\\Sdk\\GatewayCodes' => __DIR__ . '/../..' . '/src/GatewayCodes.php',
        'RocketGate\\Sdk\\GatewayParameterList' => __DIR__ . '/../..' . '/src/GatewayParameterList.php',
        'RocketGate\\Sdk\\GatewayRequest' => __DIR__ . '/../..' . '/src/GatewayRequest.php',
        'RocketGate\\Sdk\\GatewayResponse' => __DIR__ . '/../..' . '/src/GatewayResponse.php',
        'RocketGate\\Sdk\\GatewayService' => __DIR__ . '/../..' . '/src/GatewayService.php',
        'RocketGate\\Sdk\\Tests\\ACHTest' => __DIR__ . '/../..' . '/tests/ACHTest.php',
        'RocketGate\\Sdk\\Tests\\AuthOnlyTest' => __DIR__ . '/../..' . '/tests/AuthOnlyTest.php',
        'RocketGate\\Sdk\\Tests\\AuthTicketTest' => __DIR__ . '/../..' . '/tests/AuthTicketTest.php',
        'RocketGate\\Sdk\\Tests\\AuthVoidTest' => __DIR__ . '/../..' . '/tests/AuthVoidTest.php',
        'RocketGate\\Sdk\\Tests\\BaseTestCase' => __DIR__ . '/../..' . '/tests/BaseTestCase.php',
        'RocketGate\\Sdk\\Tests\\CancelPromoTest' => __DIR__ . '/../..' . '/tests/CancelPromoTest.php',
        'RocketGate\\Sdk\\Tests\\CancelTest' => __DIR__ . '/../..' . '/tests/CancelTest.php',
        'RocketGate\\Sdk\\Tests\\GatewayChecksumTest' => __DIR__ . '/../..' . '/tests/GatewayChecksumTest.php',
        'RocketGate\\Sdk\\Tests\\GenerateCrossSellTest' => __DIR__ . '/../..' . '/tests/GenerateCrossSellTest.php',
        'RocketGate\\Sdk\\Tests\\InstantUpgradeTest' => __DIR__ . '/../..' . '/tests/InstantUpgradeTest.php',
        'RocketGate\\Sdk\\Tests\\LookupTest' => __DIR__ . '/../..' . '/tests/LookupTest.php',
        'RocketGate\\Sdk\\Tests\\NonRebillUpdateToRebillTest' => __DIR__ . '/../..' . '/tests/NonRebillUpdateToRebillTest.php',
        'RocketGate\\Sdk\\Tests\\OneClickCrossMerchantWithCardHashTest' => __DIR__ . '/../..' . '/tests/OneClickCrossMerchantWithCardHashTest.php',
        'RocketGate\\Sdk\\Tests\\OneClickCrossMerchantWithPayInfoTokenTest' => __DIR__ . '/../..' . '/tests/OneClickCrossMerchantWithPayInfoTokenTest.php',
        'RocketGate\\Sdk\\Tests\\OneClickWithCardHashTest' => __DIR__ . '/../..' . '/tests/OneClickWithCardHashTest.php',
        'RocketGate\\Sdk\\Tests\\OneClickWithPayInfoTokenTest' => __DIR__ . '/../..' . '/tests/OneClickWithPayInfoTokenTest.php',
        'RocketGate\\Sdk\\Tests\\ProRatedUpgradeTest' => __DIR__ . '/../..' . '/tests/ProRatedUpgradeTest.php',
        'RocketGate\\Sdk\\Tests\\PurchaseCreditTest' => __DIR__ . '/../..' . '/tests/PurchaseCreditTest.php',
        'RocketGate\\Sdk\\Tests\\PurchaseLifetimeMembershipTest' => __DIR__ . '/../..' . '/tests/PurchaseLifetimeMembershipTest.php',
        'RocketGate\\Sdk\\Tests\\PurchaseRebillTest' => __DIR__ . '/../..' . '/tests/PurchaseRebillTest.php',
        'RocketGate\\Sdk\\Tests\\PurchaseTest' => __DIR__ . '/../..' . '/tests/PurchaseTest.php',
        'RocketGate\\Sdk\\Tests\\PurchaseTrialToLifetimeMembershipTest' => __DIR__ . '/../..' . '/tests/PurchaseTrialToLifetimeMembershipTest.php',
        'RocketGate\\Sdk\\Tests\\RebillStatusCanceledTest' => __DIR__ . '/../..' . '/tests/RebillStatusCanceledTest.php',
        'RocketGate\\Sdk\\Tests\\RebillStatusTest' => __DIR__ . '/../..' . '/tests/RebillStatusTest.php',
        'RocketGate\\Sdk\\Tests\\UpdatePersonalInformationTest' => __DIR__ . '/../..' . '/tests/UpdatePersonalInformationTest.php',
        'RocketGate\\Sdk\\Tests\\UpdateStickyMidTest' => __DIR__ . '/../..' . '/tests/UpdateStickyMidTest.php',
        'RocketGate\\Sdk\\Tests\\UploadTest' => __DIR__ . '/../..' . '/tests/UploadTest.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfab637627a3debb65828c9ab6d9a4b06::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfab637627a3debb65828c9ab6d9a4b06::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitfab637627a3debb65828c9ab6d9a4b06::$classMap;

        }, null, ClassLoader::class);
    }
}
