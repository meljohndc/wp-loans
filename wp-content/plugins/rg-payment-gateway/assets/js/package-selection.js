// Handler when the DOM is fully loaded
document.addEventListener("DOMContentLoaded", function () {

    // Utility function
    const createNode = (ele) => {
        return document.createElement(ele);
    };

    // Selectors
    const documentHead = document.getElementsByTagName('head')[0];
    const currentLoadedScript = document.getElementById('rg-gateway-embedded-form-js');
	
    // Create a script element
    const newScript = createNode('script');
    newScript.setAttribute('id', 'rg-gateway-embedded-form-js');

    // Loop through packages and onClick event replace the current script with a new one
    const packages = document.querySelectorAll('.package-holder'); 
    for (let i = 0; i < packages.length; i++) { 
        const package = packages[i];
        package.addEventListener('click', function () {

			var currentLoadedIframe = document.getElementById('rg_iframe_form');

            updateScript = (packagePrice) => {
				//currentLoadedScript.remove();
				var iframe_url 	= rg_url.iframe_url;
				var var_url 	= '';
				var_url += 'merch=' + rg_var.merch;
				var_url += '&id=' + rg_var.id;
				var_url += '&amount=' + packagePrice;
				var_url += '&purchase=' + rg_var.purchase;
				var_url += '&lang=' + rg_var.lang;
				var_url += '&currency=' + rg_var.currency;
				var_url += '&secret=' + rg_var.secret;
				var_url += '&env_mode=' + rg_var.env_mode;
				var_url += '&email=' + rg_var.email;

				var script_url 	= iframe_url + '?' + var_url;

				currentLoadedIframe.src = script_url;

                //newScript.setAttribute('src', script_url);
				//documentHead.append(newScript);
				
				//var handle 	= document.getElementById("rg_gateway_wrapper");
				//rg_load_form( handle );
			}
			
            if (this.dataset.productSku === 'KP005') {
                updateScript('110.00');
                console.log('You Selected the 5 products package', this);
            } else if (this.dataset.productSku === 'KP003') {
                updateScript('100.00');
                console.log('You Selected the 3 products package', this);
            }
            else if (this.dataset.productSku === 'KP001') {
                updateScript('49.95');
                console.log('You Selected the 1 product package', this);
            }
        });
    }
});


