<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package borrow
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php global $borrow_option; ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!-- Favicons
    ================================================== -->
    <?php borrow_custom_favicon(); ?>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<?php if(isset($borrow_option['theme_layout']) and $borrow_option['theme_layout']=="boxed_version" ){ ?>
<!-- Open .boxed-wrapper -->
<div class="boxed-wrapper">
<?php } ?>



<!-- header close -->


<div class="header <?php borrow_header_class();  ?>">
  <div class="<?php if(is_front_page() || ! is_user_logged_in()){ echo'container';}else{echo 'container-fluid';} ?>">
    <div class="row">
      <div class="col-lg-3 col-md-12 col-sm-12 col-xs-6">
        <!-- logo -->
        <div class="logo">
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <?php if($borrow_option['logo']['url'] != ''){ ?>
                <img src="<?php echo esc_url($borrow_option['logo']['url']); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">
            <?php }else{ ?>
                <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php echo get_bloginfo( 'name' ); ?>">
            <?php } ?>   
          </a>
        </div>
      </div>
      <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div id="navigation">
          <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '' ) ); ?>
        </div>
      </div>
      <div class="col-md-1 hidden-sm">
          <!-- search start-->
      </div>
    </div>
  </div>
</div>
