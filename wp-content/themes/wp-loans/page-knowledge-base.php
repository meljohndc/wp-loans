<?php
/**
 * Template Name: Knowledge Base
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
<div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="content-body-wrap <?php if(is_user_logged_in()){ echo'col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="pinside30">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                <h1 class="page-title"><?php echo get_the_title( get_option( 'page_for_posts' ) ); ?></h1>
                </div>
              </div>
               <!-- main container -->
              <div id="knowledge-base" class="row content-body">
              <?php 
                  $i=0;
                  $args = array(    
                    'paged' => $paged,
                    'post_type' => 'post',
                    );
                  $wp_query = new WP_Query($args);
                  while ($wp_query -> have_posts()): $wp_query -> the_post(); $i++; ?>
                  <?php $format = get_post_format(); ?>
                  <div class="col-lg-4 col-sm-6 col-xs-12 <?php if($i%3==1){echo 'clear';} ?>">
                    <div class=" ">
                      <div class="post-block card mb30 ">
                        <?php if($format=='audio'){ ?>
                        <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                          <?php $images = rwmb_meta( '_cmb_image2', "type=image" ); ?>
                          <?php if($images){ ?>
                            <?php foreach ( $images as $image ) { ?>
                              <?php $img = $image['full_url']; ?>
                              <div class="post-img">
                                <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img); ?>" alt=""  class="img-responsive"></a>
                              </div>
                            <?php } ?>
                          <?php }else{
                            $link_audio = get_post_meta(get_the_ID(),'_cmb_link_audio', true); 
                          }
                        } ?>
                        <iframe style="width:100%" src="<?php echo esc_url( $link_audio ); ?>"></iframe>      
                        <?php } elseif($format=='video'){ ?>
                        <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                          <?php $images = rwmb_meta( '_cmb_image2', "type=image" ); ?>
                          <?php if($images){ ?>
                            <?php foreach ( $images as $image ) { ?>
                              <?php $img = $image['full_url']; ?>
                              <div class="post-img">
                                <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img); ?>" alt=""  class="img-responsive"></a>
                              </div>
                            <?php } ?>
                          <?php }else{
                            $link_video = get_post_meta(get_the_ID(),'_cmb_link_video', true);
                            echo wp_oembed_get( $link_video );
                          }
                        } ?>   
                        <?php } elseif($format=='gallery'){ ?>
                        <div id="post-gallery">
                          <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                              <?php $image2 = rwmb_meta( '_cmb_image2', "type=image" ); ?>
                              <?php $images = rwmb_meta( '_cmb_images', "type=image" ); ?>
                              <?php if($image2){ ?>                                       
                                  <?php foreach ( $image2 as $image ) {  ?>
                                  <?php $img2 = $image['full_url']; ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img2); ?>" alt=""></a>
                                    </div> 
                                  <?php } ?>
                              <?php }elseif($images){ ?>
                                  <?php foreach ( $images as $image ) {  ?>
                                  <?php $img = $image['full_url']; ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img); ?>" alt=""></a>
                                    </div> 
                                  <?php } ?>
                              <?php } ?>
                          <?php } ?>
                        </div>   
                        <?php } elseif ($format=='image'){ ?>
                          <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                              <?php $image2 = rwmb_meta( '_cmb_image2', "type=image" ); ?>
                              <?php $images = rwmb_meta( '_cmb_images', "type=image" ); ?>
                              <?php if($image2){ ?>                                       
                                  <?php foreach ( $image2 as $image ) {  ?>
                                  <?php $img2 = $image['full_url']; ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img2); ?>" alt=""  class="img-responsive"></a>
                                    </div> 
                                  <?php } ?>
                              <?php }elseif($images){ ?>
                                  <?php foreach ( $images as $image ) {  ?>
                                  <?php $img = $image['full_url']; ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img); ?>" alt=""  class="img-responsive"></a>
                                    </div> 
                                  <?php } ?>
                              <?php } ?>
                          <?php } ?>
                        <?php }else{ ?>
                          <?php if( function_exists( 'rwmb_meta' ) ) { ?>  
                              <?php $image2 = rwmb_meta( '_cmb_image2', "type=image" ); ?>
                              <?php $images = rwmb_meta( '_cmb_images', "type=image" ); ?>
                              <?php if($image2){ ?>                                       
                                  <?php foreach ( $image2 as $image ) {  ?>
                                  <?php $img2 = $image['full_url']; ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><img src="<?php echo esc_url($img2); ?>" alt=""  class="img-responsive"></a>
                                    </div> 
                                  <?php } ?>
                              <?php }else{ ?>
                                    <div class="post-img">
                                      <a href="<?php the_permalink(); ?>" class="imghover"><?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) ); ?></a>
                                    </div>
                              <?php } ?>
                          <?php } ?>
                        <?php } ?>
                        <div class="bg-white pinside40">
                            <h2><a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a></h2>
                            <p><?php echo borrow_excerpt(12); ?></p>
                            <a href="<?php the_permalink(); ?>" class="btn-link"><?php echo esc_html_e('Read more','borrow'); ?></a> 
                          
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php endwhile;?> 
                  <div class="col-md-12 text-center col-xs-12">
                    <div class="st-pagination">
                      <?php echo borrow_pagination(); ?>
                    </div>
                  </div>
                </div>
            
                <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>

    <!-- content close -->

<?php get_footer(); ?>
