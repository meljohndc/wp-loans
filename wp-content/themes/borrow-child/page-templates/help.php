<?php
/**
* Template Name: Help
 *
 * @package borrow
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="<?php if(is_user_logged_in()){ echo'content-body-wrap col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-white pinside30">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                  <h1 class="page-title">
                    <?php the_title(); ?>
                  </h1>
                </div>
              </div>
               <!-- main container -->
              <div class="row">
                <div class="col-md-12">
                  <div class="content-body">
                    <?php
                      while (have_posts()) : the_post();
                          the_content();
                      endwhile;
                    ?>
                    <!-- faq start -->
                    <div id="help">
                    <div class="section">
                          <h6>1. Why was I charged <?php echo do_shortcode('[tags_shop_currency]' ); ?>10?</h6>
                          <p>We charge <?php echo do_shortcode('[tags_shop_currency]' ); ?>10 verification fee to verify your credit/debit card details and make sure you're not a computer robot trying to hack our system.</p>
                      </div>
                      <div class="section">
                          <h6>2. I received a message that I was granted a loan</h6>
                          <p>If you received a suspicious message or email, this might be due to the system error. In many cases, you can ignore this message or contact our customer support.</p>
                      </div>
                      <div class="section">
                          <h6>3. I applied for a loan, when will I get it?</h6>
                          <p>
                              We don't issue loans ourselves - but once you login to <?php echo do_shortcode('[tags_shop_domain]' ); ?> you'll find all available loan providers based on your selected criteria. You can also read educational content on how to find the best loan for you and use our
                              advanced calculators. <br />
                              If you want to take a loan please log in to <?php echo do_shortcode('[tags_shop_domain]' ); ?> and find a loan provider you prefer. You then have to click SELECT and you’ll be taken to the loan provider website where you have to fill the required information. It is then
                              for the loan provider itself to confirm your requested loan and they will be in touch with you.
                          </p>
                      </div>
                      <div class="section">
                          <h6>4. Cannot find my password and login details</h6>
                          <p>
                              First of all please check your email inbox and spam folder for <?php echo do_shortcode('[tags_shop_domain]' ); ?> welcome email. You can search your email inbox for keywords such as “<?php echo do_shortcode('[tags_shop_domain]' ); ?>”. If you are not able to locate your welcome email with login credentials
                              contact our customer support via email: <?php echo do_shortcode('[tags_shop_email]' ); ?>.
                          </p>
                      </div>
                    </div>
                    <!-- tc end -->
                  </div>
                </div>
              </div>
              <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
