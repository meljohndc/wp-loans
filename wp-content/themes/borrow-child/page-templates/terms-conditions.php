<?php
/**
* Template Name: Terms & Conditions
 *
 * @package borrow
 */
global $borrow_option;
get_header(); ?>

<div class="content-wrap">
  <div class="<?php if(is_user_logged_in()){ echo'container-fluid';}else{echo 'container';} ?>">
    <div class="row flex-column-reverse flex-md-row">
      <?php if(is_user_logged_in()){ ?>
      <div class="content-side-wrap col-xl-2 col-lg-3 col-md-4">
        <div class="content-side">
          <?php get_sidebar();?>
        </div>
      </div>
      <?php } ?>
      <div class="<?php if(is_user_logged_in()){ echo'content-body-wrap col-xl-10 col-lg-9 col-md-8';}else{echo 'col-12';} ?> pt-4">
        <div class="row">
          <div class="col-md-12 content-head">
            <div class="page-breadcrumb">
              <?php if($borrow_option['bread-switch']==true){ ?>
                <ol class="breadcrumb">
                    <?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
                </ol>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="bg-white pinside30">
              <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                  <h1 class="page-title">
                    <?php the_title(); ?>
                  </h1>
                </div>
              </div>
               <!-- main container -->
              <div class="row">
                <div class="col-md-12">
                  <div class="content-body">
                    <?php
                      while (have_posts()) : the_post();
                          the_content();
                      endwhile;
                    ?>
                    <!-- tc start -->
                    <div id="terms-and-conditions">
                      <h4>About us</h4>
                      <p><?php echo do_shortcode('[tags_shop_domain]' ); ?> is a website which provides a Smart credit analytics platform. </p>

                      <p><?php echo do_shortcode('[tags_shop_domain]' ); ?> is a trading name of <?php echo do_shortcode('[tags_company_name]' ); ?>. We provide a service which enables you to Compare loan providers based on your personal needs, access educational content and find money-saving tips. All this will enable you to make an informed decision before proceeding to your chosen loan provider for further information.</p>

                      <p>We do not provide financial or other advice in relation to the products or services compared, nor do we provide a recommendation or endorsement.</p>

                      <p>When it comes to our providers, we don’t choose favourites. We offer impartial and independent comparison and our results are ranked using multiple variables, such as Annual Percentage Rate (APR). We do not guarantee that we will list all available loan providers in the market, but we will do our best to provide you a detailed and broad comparison platform.</p>

                      <p>Best offer is displayed based on selection of multiple variables such as APR, Trustpilot score, response time and alike, and is the offer which is rated best across all the variables.</p>

                      <p><?php echo do_shortcode('[tags_shop_domain]' ); ?> is operated for commercial purposes.</p>
                      <h4>Summary </h4>
                      <p>We set out below the important points for you to note when using our website. These summary points are not part of our Terms and Conditions and are for reference only. Before using our website, we recommend that you read all of our Terms and Conditions to ensure that you are happy with them. </p>

                      <p>You must only use the <?php echo do_shortcode('[tags_shop_domain]' ); ?> website for your personal use. You must provide accurate and complete information when using this website so that we can provide appropriate price comparison. </p>

                      <p>Nothing in these Terms and Conditions will constitute an offer or promotion for services or products provided by us. However, in case a loan provider is running a special promotion, we might inform you about, or promote, this deal too, as long as it benefits you. </p>

                      <p>All information data and copyright material contained on this website must not be reproduced or used without our consent.</p>

                      <p>We provide a loan aggregation platform where you propose to enter into a separate agreement or policy with a loan provider, you should check their terms and conditions to ensure that you are comfortable with them.</p>

                      <h4>Parties to This Agreement and Consideration </h4>
                      <p>The parties to this Agreement ("Agreement") are You ("you") and the owner of <?php echo do_shortcode('[tags_shop_domain]' ); ?> (<?php echo do_shortcode('[tags_company_name]' ); ?>). As used in this Agreement, the terms "we" and "us" are used interchangeably to refer to <?php echo do_shortcode('[tags_company_name]' ); ?> and its operators of the Website (sometimes referred to as <?php echo do_shortcode('[tags_shop_domain]' ); ?>). By further accessing the Website or service available at or in association with the website, and for other good and valuable consideration, the sufficiency of which is acknowledged by you and <?php echo do_shortcode('[tags_company_name]' ); ?>. You hereby agree to be bound by all the terms and conditions set forth in this Agreement.</p>
                      <h4>1. General </h4>
                      <ol>
                        <li>1.1. These Terms and Conditions apply to your use of this website.</li>
                        <li>1.2. By using this website you agree to these Terms and Conditions. If you do not wish to be bound by these Terms and Conditions you should not use this website.</li>
                        <li>1.3. We may change these Terms and Conditions from time to time. We will give you notice of any changes by posting those changes on our website. We will not vary any terms and conditions which you previously agreed whilst using this website. However, any amended Terms and Conditions will apply to your use of this website from the date they are posted and if you do not agree with the amended Terms and Conditions, you are entitled to stop using this website at that time. </li>
                      </ol>
                      <h4>2. Definitions</h4>
                      <ol>
                        <li>2.1. "you" and "your" means the person who proposes to use or is using this website.</li>
                        <li>2.2. "we" "us" and "our" means <?php echo do_shortcode('[tags_shop_domain]' ); ?> is a trading name of <?php echo do_shortcode('[tags_company_name]' ); ?> of <?php echo do_shortcode('[tags_shop_address_local]' ); ?>  and any other person or business to whom we may legally transfer our rights under these Terms and Conditions. You are entitled to stop using this website if we transfer our rights under these Terms and Conditions to a third party. </li>
                        <li>2.3. "website" means a site on the world wide web located at <?php echo do_shortcode('[tags_shop_domain]' ); ?>.</li>
                        <li>2.4. "product or service provider" means the manufacturer, supplier, distributor or provider of any service or product advertised on this website or made available via this website.</li>
                        <li>2.5. "offer" means a price provided by a product or service provider for comparison with other offers on the basis of the information you have provided. In providing you with an offer we are not able to guarantee the availability of the product, the service or the price set out as you will need to deal with the product or service provider directly to make a purchase.</li>
                        <li>2.6. “Your documents” means the online document repository service provided by us on this website.</li>
                      </ol>
                      <h4>3. Use of this website</h4>
                      <ol>
                        <li>3.1. By using this website you agree that:
                          <ol>
                            <li>3.1.1. You will not do anything that affects the integrity or security of this website or causes or may cause harm, damage or unreasonable inconvenience to other users of this website or us; and</li>
                            <li>3.1.2. You will not gather, extract, download, reproduce and/or display or advertise on any other website, other online or off-line service or otherwise, any material on or from this website. </li>
                          </ol>
                        </li>
                        <li>3.2. If you breach any of the clauses set out at 3.1 above, we may take such action as we deem appropriate, including denying you access to this website, bringing legal proceedings against you and disclosing such information to appropriate legal and/or regulatory bodies.</li>
                        <li>3.3. You will have to register before you can use our service. If you just registered, you get a test period until the end of the current month for only <?php echo do_shortcode('[tags_shop_currency]' ); ?> <?php echo do_shortcode('[tags_shop_initial_price]' ); ?>. Once the payment has been completed, you will receive an order confirmation via email.  </li>
                      </ol>
                      <h4>4. Billing</h4>
                      <ol>
                        <li>4.1. <span class="text-uppercase"><?php echo do_shortcode('[tags_shop_domain]' ); ?><?php echo do_shortcode('[tags_shop_phone]' ); ?></span> may appear on your credit card, bank statement, or phone bill for all applicable charges. If multiple venues are joined utilizing any payment method, your statement will list each individual purchase comprising the transaction. <?php echo do_shortcode('[tags_shop_domain]' ); ?> may include other information on your statement based on credit card association, telephone regulation and any other mandated rules and regulations.</li>
                        <li>4.2. After the initial test period, please see above clause 3.3, you will automatically be enrolled to a full Membership at the price and terms chosen upon signup, until you no longer wish to be registered. You can terminate your subscription at any time by sending an email directly to our <?php echo do_shortcode('[tags_shop_email]' ); ?>.</li>
                      </ol>
                      <h4>5. Automatic Recurring Billing</h4>
                      <ol>
                        <li>5.1. In accordance with these Terms and Conditions, subscription fees will be automatically renewed at or after the end of the original term selected, for a similar period of time and for a similar or lower amount, unless notice of cancellation is received from you. From time to time we enroll our loyal members in a loyalty program where the randomly chosen members will be granted a discounted membership price on the next payment.</li>
                        <li>5.2. Unless and until this agreement is cancelled in accordance with the terms hereof, you hereby authorize <?php echo do_shortcode('[tags_shop_domain]' ); ?> to charge your chosen payment method to pay for the ongoing cost of the subscription.</li>
                        <li>5.3. By agreeing to these Terms and Conditions, you accept being enrolled in a test period until the end of the current month for only <?php echo do_shortcode('[tags_shop_currency]' ); ?> <?php echo do_shortcode('[tags_shop_initial_price]' ); ?>, and acknowledges that unless cancelled within due time, you will automatically be enrolled in the monthly membership at the price and terms chosen upon enrolling to the test period. Billing notifications will be sent from <?php echo do_shortcode('[tags_shop_email]' ); ?>. This charge will appear on your credit card statement as <span class="text-uppercase"><?php echo do_shortcode('[tags_shop_domain]' ); ?><?php echo do_shortcode('[tags_shop_phone]' ); ?></span>.</li>
                      </ol>
                      <h4>6. Your Obligations</h4>
                      <ol>
                        <li>6.1. You must only use the <?php echo do_shortcode('[tags_shop_domain]' ); ?> website for your personal use. </li>
                        <li>6.2. It is your responsibility to ensure that all information you supply to us or enter onto this website is complete and accurate, so please double-check your information before submitting it.</li>
                        <li>6.3. You will need to answer a number of questions in order to compare or obtain an offer for any product or service. These questions are designed to ensure that we have all of the information necessary to provide you with accurate, appropriate and timely information relating to the loan providers.</li>
                        <li>6.4. If you are purchasing or using a loan provider's website directly you should also read their terms and conditions. The loan provider's terms and conditions will cover any transaction or agreement between you and the loan provider, whereas these Terms and Conditions agreed with us relate only to your use of this website.</li>
                      </ol>
                      <h4>7. Cancellation</h4>
                      <ol>
                        <li>7.1. If you wish to terminate Your Membership, Your subscription must be canceled at least 7 days prior to the end of the current term (or within Your trial period in the case of trial Membership). </li>
                        <li>7.2. You agree that if You do not send the Company notice of cancellation of Your Membership at least SEVEN (7) DAYS from the expiration of Your Membership term (including any free or promotional membership terms), or within Your trial period in the case of trial Membership the Company shall, with the full authorization You hereby provide, automatically and without further notice, renew Your Membership to <?php echo do_shortcode('[tags_shop_domain]' ); ?>. You will receive an email from <a href="mailto:<?php echo do_shortcode('[tags_shop_email]' ); ?>"><?php echo do_shortcode('[tags_shop_email]' ); ?></a> before the end of Your trial period, confirming that, unless cancelled, Your Membership will renew to the full subscription once the trial period has expired.</li>
                        <li>7.3. All cancellations received by the Company at least SEVEN (7) DAYS from the expiration of Your Membership will be effective upon receipt and The Company may, at any time and at its sole discretion and without cause or the providing of any reason, cancel any Membership.</li>
                        <li>7.4. You hereby acknowledge and agree that if You cancel Your Membership, or if Your Membership is cancelled by the Company, Your password will be removed from the system at the end of the then current Membership period and that You will be entitled to receive the full benefits of Your Membership until the end of such Membership period. You shall not be entitled to any pro-rated or partial refund if You cancel Your Membership before the end of the then current Membership period for any reason.</li>
                        <li>7.5. In order to cancel Your <?php echo do_shortcode('[tags_shop_domain]' ); ?> Membership, please call us at <?php echo do_shortcode('[tags_shop_phone]' ); ?> or email us at <a href="mailto:<?php echo do_shortcode('[tags_shop_email]' ); ?>"><?php echo do_shortcode('[tags_shop_email]' ); ?></a>.</li>
                      </ol>
                      <h4>8. Refunds</h4>
                      <ol>
                        <li>8.1. Refunds for purchases or recurring charges may be requested by contacting customer support at “<?php echo do_shortcode('[tags_shop_phone]' ); ?>” or email us at <a href="mailto:<?php echo do_shortcode('[tags_shop_email]' ); ?>"><?php echo do_shortcode('[tags_shop_email]' ); ?></a>.</li>
                        <li>8.2. Refunds or credits will not be issued for partially used Memberships. Cancellation for all future recurring billing may be requested in accordance with Section 7 - Cancellation. <?php echo do_shortcode('[tags_shop_domain]' ); ?> reserves the right to grant a refund or a credit applicable to purchases to the Website at its discretion. The decision to refund a charge does not imply the obligation to issue additional future refunds. Should a refund be issued by <?php echo do_shortcode('[tags_shop_domain]' ); ?> for any reason, it will be credited solely to the payment method used in the original transaction. <?php echo do_shortcode('[tags_shop_domain]' ); ?> will not issue refunds by cash, cheque, money transfer or other means of payment. The refund will be processed immediately by <?php echo do_shortcode('[tags_shop_domain]' ); ?>, depending on the payment method used in the original transaction, depositing the funds may take 3-5 business days.</li>
                      </ol>

                      <h4>9. Cardholder Disputes/Chargebacks</h4>
                      <p>All chargebacks are thoroughly investigated and may prevent future purchases with <?php echo do_shortcode('[tags_shop_domain]' ); ?> given the circumstances. Fraud claims may result in <?php echo do_shortcode('[tags_shop_domain]' ); ?> contacting Subscriber's issuer to protect Subscriber and prevent future fraudulent charges to Subscriber card.</p>
                      <h4>10. Our Services</h4>
                      <ol>
                        <li>10.1. We provide a service which enables you to compare a number of prices for loans and make a decision based on those comparisons. </li>
                        <li>10.2. We do not provide financial, investment or other advice in relation to loans compared, nor do we provide a recommendation or endorsement of the different providers. We might show ratings of the different providers, however this is captured from reliable source(s) (I.e. TrustPilot) and will never be our own recommendation. </li>
                        <li>10.3. The information you provide us is used to filter out loan providers matching your criteria. Therefore, it is important that you double-check all information to ensure that it is complete and accurate. We might pass on the information to the loan providers in certain cases, however we seek to ensure that we accurately pass on the information you have provided to the loan provider for the purpose of providing a price comparison.</li>
                      </ol>
                      <h4>11. Validity of offer</h4>
                      <ol>
                        <li>11.1. We do not control the price, length of offers, or special conditions relating to price comparison offers. These are set by the loan providers. If you obtain an offer and would like to see how long it is valid for or any special conditions relating to it, please click through the links contained on our website to the loan provider's website. Loan providers' prices, rates and offer times are updated and amended on a regular basis so our price comparisons are subject to change.</li>
                        <li>11.2. In order to purchase any of the loans described on our website you must contact the loan provider. When an offer is shown on this website you will also find a direct link to the loan provider's website. For some loans you may need to or choose to complete your transaction over the telephone. The details of the providers are listed on their website(s).</li>
                      </ol>
                      <h4>12. Delivery of offer </h4>
                      <ol>
                        <li>12.1. You should receive the results of our loan provider comparison online. However, we rely on the availability of third party services and general performance of the world wide web. At certain times some or all online offers may not be available to you due to problems with third party websites or the worldwide web.</li>
                        <li>12.2. Whilst we strive to provide as comprehensive and easy to use a service as possible, the requirements for your offer may not be met if such requirements are unusual, "non standard" or "high risk". If this is the case you should contact a provider directly to discuss your requirements.</li>
                        <li>12.3. Each loan provider will have a different approach to the provision of their offers and may not make offers available for all of their products. </li>
                      </ol>
                      <h4>13. Responsibility for content</h4>
                      <ol>
                        <li>13.1. We will use our best endeavours to provide you with a quality service and virus free website. However, we are not responsible for computer viruses or other computer related problems you suffer as a result of using this website, which are beyond our reasonable control. We recommend that you use your own appropriate virus checking software.</li>
                        <li>13.2. We are not responsible for any commentary, opinions, ratings or other postings on the website by third parties.</li>
                        <li>13.3. Providers and third-party websites are responsible for failings or errors on their websites and may have a separate duty of care to you.</li>
                      </ol>
                      <h4>14. Intellectual property</h4>
                      <ol>
                        <li>14.1. All information, data and copyright material contained on this website, including any trademarks (whether officially registered or unregistered), trade names, brands, logos and devices belong to us or to people whom we have given ownership or permission to use such information data and/or copyright material. You must not use such information or copyright material unless you have written permission from us or the owner to do so.</li>
                        <li>14.2. You may download or copy the content and other downloadable items displayed on this website for personal non-commercial use only, (i.e. for personal educational purposes), provided that you are not otherwise breaching these Terms and Conditions.</li>
                      </ol>
                      <h4>15. Privacy policy</h4>
                      <ol>
                        <li>15.1. We will comply with all applicable data protection legislation which is in force in respect of the personal data we collect from you. </li>
                        <li>15.2. Additionally, we will observe the terms of our <a href="/privacy-policy/">Privacy Policy</a>.</li>
                        <li>15.3. If you decide to select or make further queries about an offer through this website, you will be directed to the provider's website. Those providers will be subject to their own terms and conditions and each provider may have a different privacy policy from ours.</li>
                      </ol>
                      <h4>16. Third party websites</h4>
                      <ol>
                        <li>16.1. This website contains links to websites operated by third parties. The operation and content of those websites are determined by those third parties and will be governed by separate terms and conditions. We do not accept responsibility or liability for the content of any third-party website, as this is outside of our control.</li>
                        <li>16.2. Links are provided for convenience only and inclusion of any links should not be taken to imply endorsement in any way of the site to which it links. </li>
                        <li>16.3. You may wish to take the following steps when selecting a product or service from a third-party website:
                          <ol>
                            <li>16.3.1. Read and ensure that you understand the terms and conditions of their website and terms and conditions of any product or service you might be agreeing with them;</li>
                            <li>16.3.2. Clarify and/or check your understanding of relevant terms and conditions by seeking independent advice, for example if the proposed transaction is of significant financial value to you. </li>
                          </ol>
                        </li>
                      </ol>
                      <h4>17. Termination</h4>
                      <ol>
                        <li>17.1. We may issue a warning, temporarily suspend, permanently suspend or terminate your right to use this website if you:
                          <ol>
                            <li>17.1.1. Substantially breach any of these Terms and conditions;</li>
                            <li>17.1.2. Infringe or violate or attempt to infringe or violate any rights of any other user(s) of this website and/or third parties in connection with this website; or</li>
                            <li>17.1.3. If you are using this website to commit or attempt to commit a criminal offence.</li>
                          </ol>
                        </li>
                      </ol>
                      <h4>18. Jurisdiction and Enforceability</h4>
                      <ol>
                        <li>18.1. If any provision of these Terms and Conditions are held by a competent authority to be invalid or unenforceable, the remaining provisions of these Terms and Conditions will not be affected and will remain valid.</li>
                        <li>18.2. In the event of a dispute in connection with or arising out of these Terms and Conditions, United Kingdom law will apply.</li>
                        <li>18.3. These Terms and Conditions will only apply as between us and you. Unless as otherwise stated in these Terms and Conditions no other person may benefit or rely upon these Terms and Conditions.</li>
                      </ol>
                      <h4>19. Complaints procedure</h4>
                      <h6>SERVICE STANDARDS</h6>
                      <p>We aim to provide a high standard of service to all our users. In case you have a complaint, we will make every effort to settle your complaint quickly and fairly.</p>

                      <p><strong>How to contact us:</strong><br>
                        If you have a complaint about our service, please contact us:</p>

                      <p><strong>By email:</strong><br>
                        Please email us at <?php echo do_shortcode('[tags_shop_email]' ); ?><br>
                        <strong>By telephone:</strong><br>
                        Please contact us on <?php echo do_shortcode('[tags_shop_phone]' ); ?></p>

                      <p>Always include your name, postal address, and email address. Please also include your offer reference (loan provider name, loan amount, age) and telephone number and why you are unhappy in as much detail as you can. This will help us to respond to you quickly. If we do not have enough information to investigate your complaint, we will try and contact you to ask for further details.</p>

                      <h6>COMPLAINT RESPONSE TIMESCALES</h6>
                      <p>All complaints will be answered within two (2) working days. </p>

                      <h6>COMPLAINTS ABOUT THIRD PARTY PROVIDERS </h6>
                      <p>If your complaint is about a loan provider which you have used through our website, then you can complain directly to the relevant loan provider responsible. We cannot answer complaints on behalf of a loan provider or accept responsibility for any such complaints. We recommend that you visit the website of the loan provider and follow their complaints procedure if you wish to make a complaint against them.</p>

                      <h6>ONLINE DISPUTE RESOLUTION PLATFORM</h6>
                      <p>If you wish to raise a complaint about a product or service bought online you can also do so through the European Commission’s Online Dispute Resolution platform Online dispute resolution. They will advise you of the right body to resolve your complaint. </p>

						<h6>COMPLAINTS ABOUT YOUR BANK</h6>
<p>Legislation ensures that there is a written complaints process which describes how you can complain to your bank. If you haven’t received a response within 8 weeks, or you are still unhappy you might be able to complain to <a href="https://www.financial-ombudsman.org.uk/" target="_blank">The Financial Ombudsman Service</a>. The Financial Ombudsman Service is free and assists with any dispute between consumer and businesses that provide financial services.</p>
                    </div>
                    <!-- tc end -->
                  </div>
                </div>
              </div>
              <!-- content close -->
            </div>
          </div>
        </div>
       
      </div>
    </div>
  </div>
</div>


<?php get_footer(); ?>
