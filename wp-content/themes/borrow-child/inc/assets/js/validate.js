(function ($) {

    "use strict";
    var language;

    $(document).ready(function () {
        getLanguage();
        customValidation();
        replaceCharacters();
        showErrorMessages();
        removeErrorMessage();
        onFormSubmit();
    });

    /**
     * Adds custom validators for fields
     */
    var customValidation = function () {
        window.Parsley
            .addValidator('name', {
                validateString: function (value) {
                    var filter = /^[a-z'\-\sæøåäöÆØÅÄÖàâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇẞßĄąĆćĘęŁłŃńÓóŚśŹźŻżÑñ]{2,}$/i;
                    return filter.test(value);
                }
            })
            .addValidator('address', {
                validateString: function (value) {
                    var filter = /[a-z0-9',\.\-\sæøåäöÆØÅÄÖàâäèéêëîïôœùûüÿçÀÂÄÈÉÊËÎÏÔŒÙÛÜŸÇẞßĄąĆćĘęŁłŃńÓóŚśŹźŻżÑñ]+/i;
                    return filter.test(value);
                }
            })
            .addValidator('zip', {
                validateString: function (value, country) {
                    return validateZip(value, country);
                }
            })
            .addValidator('phone', {
                requirementType: 'string',
                validateString: function (value) {
                    return validatePhone(value);
                }
            })
            .addValidator('ccNumber', {
                validateString: function (value) {
                    return ($.payment.validateCardNumber(value) && !bannedCard(value));
                }
            })
            .addValidator('ccCvv', {
                validateString: function (value) {
                    return $.payment.validateCardCVC(value);
                }
            })
            .addValidator('ccExpiresMonth', {
                validateString: function (value) {
                    var filter = /\b(0[1-9]|1[0-2])\b/;
                    return filter.test(value);
                }
            })
            .addValidator('ccExpiresYear', {
                validateString: function (value) {
                    return validateCcExpiresYear(value);
                }
            });
    };

    /**
     * Gets the language
     */
    var getLanguage = function () {
        language = $('html').attr('lang');
        if (!language) {
            language = 'en';
        }
    };

    /**
     * Initialize qtip tooltip message
     * and displays the message on input validation error
     */
    var showErrorMessages = function () {
        window.Parsley.on('field:error', function () {
            var element = this.$element;
            var positionAt = element.attr('data-tooltip-at');
            var positionMy = element.attr('data-tooltip-my');
            var customClasses = element.closest("form").attr('data-qtip-classes');
            var content;
            var ready;
            if (element[0].id === "terms") {
                content = 'Please accept our Terms & Conditions before continuing';
                ready = true;
            } else {
                content = element.next('.parsley-errors-list').text();
                ready = false;
            }

            if (!positionAt) {
                positionAt = 'bottom left';
            }
            if (!positionMy) {
                positionMy = 'top left';
            }

            if (!customClasses) {
                customClasses = '';
            }

            element.qtip({
                content: content,
                show: {
                    ready: ready,
                    event: 'click mouseenter'
                },
                hide: {
                    event: 'mouseleave'
                },
                position: {
                    my: positionMy,
                    at: positionAt,
                    target: element
                },
                style: {
                    classes: customClasses
                }
            });
        });
    };

    /**
     * Removes qtip tooltip error message on field validation success
     */
    var removeErrorMessage = function () {
        window.Parsley.on('field:success', function () {
            var element = this.$element;
            element.qtip('destroy');
        });
    };


    /**
     * Disable button on form submit to prevent double submission
     */
    var disableSubmitButton = function (element) {
        var button = element.find('button.btn-txt');
        if (button.length) {
            var buttonText = button.data("alt-text");
            button.text(buttonText);
            button.attr('disabled', true);
        }
    };


    /**
     * Form submitting
     */
    var onFormSubmit = function () {
        var forms = $('form');
        if (forms.length) {
            forms.each(function () {
                $(this).parsley().on('form:submit', function () {
                    var form = $(this.$element);
                    return handleForms(form);
                });
            });
        }
    };

    /**
     * Replaces special characters
     */
    var replaceCharacters = function () {
        window.Parsley.on('field:validate', function () {
            var element = this.$element;
            var value = element.val();
            if (value) {
                value = value.replace(/æ/g, 'ae');
                value = value.replace(/ø/g, 'oe');
                value = value.replace(/å/g, 'aa');
                value = value.replace(/ä/g, 'ae');
                value = value.replace(/ö/g, 'oe');
                value = value.replace(/Æ/g, 'Ae');
                value = value.replace(/Ø/g, 'Oe');
                value = value.replace(/Å/g, 'Aa');
                value = value.replace(/Ä/g, 'Ae');
                value = value.replace(/Ö/g, 'Oe');

                value = value.replace(/à/g, 'a');
                value = value.replace(/â/g, 'a');
                value = value.replace(/à/g, 'a');
                value = value.replace(/è/g, 'e');
                value = value.replace(/é/g, 'e');
                value = value.replace(/ê/g, 'e');
                value = value.replace(/ë/g, 'e');
                value = value.replace(/î/g, 'i');
                value = value.replace(/ï/g, 'i');
                value = value.replace(/ô/g, 'o');
                value = value.replace(/œ/g, 'oe');
                value = value.replace(/ù/g, 'u');
                value = value.replace(/û/g, 'u');
                value = value.replace(/ü/g, 'u');
                value = value.replace(/ÿ/g, 'y');
                value = value.replace(/ç/g, 'c');
                value = value.replace(/À/g, 'A');
                value = value.replace(/Â/g, 'A');
                value = value.replace(/É/g, 'E');
                value = value.replace(/È/g, 'E');
                value = value.replace(/Ê/g, 'E');
                value = value.replace(/Ë/g, 'E');
                value = value.replace(/Î/g, 'I');
                value = value.replace(/Ï/g, 'I');
                value = value.replace(/Ô/g, 'O');
                value = value.replace(/Œ/g, 'Oe');
                value = value.replace(/Ù/g, 'U');
                value = value.replace(/Û/g, 'U');
                value = value.replace(/Ü/g, 'U');
                value = value.replace(/Ÿ/g, 'Y');
                value = value.replace(/Ç/g, 'C');
                value = value.replace(/ẞ/g, 'Sz');
                value = value.replace(/ß/g, 'ss');

                //Polish
                value = value.replace(/Ą/g, 'A');
                value = value.replace(/ą/g, 'a');
                value = value.replace(/Ć/g, 'C');
                value = value.replace(/ć/g, 'c');
                value = value.replace(/Ę/g, 'E');
                value = value.replace(/ę/g, 'e');
                value = value.replace(/Ł/g, 'L');
                value = value.replace(/ł/g, 'l');
                value = value.replace(/Ń/g, 'N');
                value = value.replace(/ń/g, 'n');
                value = value.replace(/Ó/g, 'O');
                value = value.replace(/ó/g, 'o');
                value = value.replace(/Ś/g, 'S');
                value = value.replace(/ś/g, 's');
                value = value.replace(/Ź/g, 'Z');
                value = value.replace(/ź/g, 'z');
                value = value.replace(/Ż/g, 'Z');
                value = value.replace(/ż/g, 'z');

                //spanish
                value = value.replace(/Ñ/g, 'Ny');
                value = value.replace(/ñ/g, 'ny');

                element.val(value);
            }

        });
    };

    /**
     * Validates zip based on the country
     * @param value
     * @param country
     * @returns boolean
     */
    var validateZip = function (value, country) {
        var filter = '';
        switch (country) {
            case "fr":
                filter = /^[0-9]{5}$/;
                break;
            case "de":
                filter = /^[0-9]{5}$/;
                break;
            case "fi":
                filter = /^[0-9]{5}$/;
                break;
            case "jp":
                filter = /^\d{3}-\d{4}$/;
                break;
            case "se":
                filter = /^\d{3}\s?\d{2}$/;
                break;
            case "au":
                filter = /^[0-9]{4}$/;
                break;
            case "gb":
                filter = /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/;
                break;
            case "dk":
                filter = /^[0-9]{4}$/;
                break;
            case "no":
                filter = /^[0-9]{4}$/;
                break;
            case "re":
                filter = /^974\d{2}$/;
                break;
            case "be":
                filter = /^[0-9]{4}$/;
                break;
            case "us":
                filter = /^\d{5}([ \-]\d{4})?$/;
                break;
            default:
                filter = /^[a-z0-9\-\s]{3,20}$/i;
        }
        return filter.test(value);
    };

    /**
     * Validates phone number
     * @param value
     * @returns boolean
     */
    var validatePhone = function (value) {
        var filter = /^[0-9\+]{6,20}$/;
        return filter.test(value);
    };

})(jQuery);