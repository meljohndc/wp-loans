<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>
<?php global $borrow_option; ?>
<div class="login-wrapp text-center">
  <?php if($borrow_option['logo']['url'] != ''){ ?>
    <img src="<?php echo esc_url($borrow_option['logo']['url']); ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">
  <?php }else{ ?>
    <h6><?php esc_html_e( 'Login', 'woocommerce' ); ?></h6>
  <?php } ?>  
    

  <form class="woocommerce-form woocommerce-form-login login needs-validation" method="post" novalidate>

  <?php do_action( 'woocommerce_login_form_start' ); ?>

  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label class="d-none" for="username"><?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" placeholder="<?php esc_html_e( 'Username or email address', 'woocommerce' ); ?>" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" required/><?php // @codingStandardsIgnoreLine ?>
    <span class="invalid-tooltip">This field is required</span>
  </p>



  <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
    <label class="d-none" for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
    <input class="woocommerce-Input woocommerce-Input--text input-text form-control" placeholder="<?php esc_html_e( 'Password', 'woocommerce' ); ?>" type="password" name="password" id="password" autocomplete="current-password" />
  </p>

  <?php do_action( 'woocommerce_login_form' ); ?>

  <p class="form-row mb-2">
    <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
    <button type="submit" class="woocommerce-form-login__submit btn btn-primary" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Log in', 'woocommerce' ); ?></button>
  </p>

  <?php do_action( 'woocommerce_login_form_end' ); ?>

  </form>
</div>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>

