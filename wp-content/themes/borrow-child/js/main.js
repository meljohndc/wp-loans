// main file for custom js functions

// form register validation
function registerFormValidation() {
    window.addEventListener('load', function() {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
}
// force native validation
function forceValidation() {
    setTimeout(function() {
        jQuery('p#billing_address_1_field, p#billing_city_field, p#billing_postcode_field').addClass("validate-required");
    }, 100);
}

// set flag for form validation
function checkFormError() {
    var checkout_form = jQuery('form.checkout');
    checkout_form.on('checkout_place_order', function () {
        if (jQuery('#confirm-order-flag').length == 0) {
            checkout_form.append('<input type="hidden" id="confirm-order-flag" name="confirm-order-flag" value="1">');
        }
        return true;
    });
}
function checkoutFormValidate() {
    jQuery(document.body).on('checkout_error', function () {
        var error_count = jQuery('.woocommerce-error li').length;
        if (error_count == 1) {
            console.log("from validated successfully");
            jQuery('body').hide();
            jQuery('#confirm-order-flag').val('');
            window.location.replace('../payment');
            // jQuery('#place_order').trigger('click');
        }else{
            jQuery('.woocommerce-error li').each(function(){
                var error_text = jQuery(this).text();
                var trimText = jQuery.trim(error_text);
                console.log(trimText);
                if (trimText == 'custom_notice'){
                    jQuery(this).css('display', 'none');
                }
            });
        }
    });
}

// load custom functions on page ready
jQuery( document ).ready(function() {
    'use strict';
    registerFormValidation();
    checkFormError();
    checkoutFormValidate();
    forceValidation();
});