<?php
/**
 * Template Name: Payment Page
 */
get_header(); ?>

	<!-- content begin -->
<div class=""><!-- main container -->
  <div class="container min-vh pb-5">
    <div class="row align-items-center">
      <div class="col-md-12">
        <div class="bg-white pinside40 mb-3">
          
          <div class="row">
            <div class="col-sm-12 col-xs-12">
            <h1><?php _e("Payment Details", "woocommerce"); ?></h1>
            <div class='paymentTotal'>
              <?php _e("Today's charge", "woocommerce"); ?>
              <?php echo wc_price( $woocommerce->cart->total ); ?>
            </div>
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php while ( have_posts() ) : the_post(); ?>
                	
        					<?php the_content(); ?>
        					
        				<?php endwhile; // End of the loop. ?>
              </div>                   
            </div>
          </div>
        </div>
        <div class="text-center">
          <?php echo do_shortcode('[tags_company_name]'); ?>,
          Registration #<?php echo do_shortcode('[tags_shop_registration]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_local]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_city]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_postcode]'); ?>,
          <?php echo do_shortcode('[tags_shop_address_country]'); ?>,
          <?php echo do_shortcode('[tags_shop_phone]'); ?>
        </div>
	    </div>
    </div>
  </div>
</div>
<!-- content close -->
<style>
#sticky-wrapper, .header, .footer{display:none;}
/*Hide Billing Details*/
#customer_details,
#order_review_heading,
.woocommerce-checkout-review-order-table,
label[for="payment_method_stripe"],
#stripe-payment-data p{
  display:none;
}
.woocommerce #payment #place_order, .woocommerce-page #payment #place_order {
    float: none;
    background-color: var(--accent);
    margin: 20px auto;
    display: block;
}
#add_payment_method #payment, .woocommerce-cart #payment, .woocommerce-checkout #payment{
  background:white;
}
p.form-row.validate-required{
  margin:20px 0 30px;
}
/*Stripe elements*/

.wc-stripe-elements-field{
  width: 100%;
  height: 45px;
  padding: 10px 16px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-image: none;
  background-color: #fff;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
  box-shadow: inset 0 0px 0px rgba(0, 0, 0, .075);
  margin-bottom: 10px;
  border: 2px solid #e6ecef;
}
</style>
<?php
	function wploan_post_billing_details() {?>
		<script>
			function postBillingDetails() {
        jQuery('input#billing_first_name').val('<?php echo ($_POST['firstname'] ?? null) ?>');
        jQuery('input#billing_last_name').val('<?php echo ($_POST['lastname'] ?? null) ?>');
        jQuery('input#billing_email').val('<?php echo ($_POST['email'] ?? null) ?>');
        jQuery('input#billing_phone').val('<?php echo ($_POST['phone'] ?? null) ?>');
        jQuery('input#billing_address_1').val('<?php echo ($_POST['address1'] ?? null) ?>');
        jQuery('input#billing_address_2').val('<?php echo ($_POST['address2'] ?? null) ?>');
        jQuery('input#billing_postcode').val('<?php echo ($_POST['zipcode'] ?? null) ?>');
        jQuery('input#billing_city').val('<?php echo ($_POST['city'] ?? null) ?>');
      }
      // load custom functions on page ready
      jQuery( document ).ready(function() {
          postBillingDetails();
      });
		</script>
	<?php }
	add_action( 'wp_footer', 'wploan_post_billing_details' );  
?>
<?php get_footer(); ?>


